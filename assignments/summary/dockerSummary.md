# Docker
- Docker is a platform for developing,shipping,running your application.
- It suits all programming language.
- Provides responsive infrastructure to our applications so as to fastly deliver project.

# Container

- Provides devloper with all of the functionality it needs such as libraries and other dependencies inside one package.

- It is like a unit for distributing and testing your application.

- These docker container can be easily shared and run on real servers.


# Architecture of Docker

**Docker Engine**

- It is installed on the local system. 
- There are 3 components in the Docker Engine-
1. 	Server
    A server which is a type of long-running program called a daemon process. 
2.	Rest API: 
    A REST API provides interfaces to programs to talk to the server and instruct it what to do.
3.	Command Line Interface (CLI)

**Docker Client**

- Users interact with Docker through a client from local computer. 
- Client communicates with more than one server at a time in Docker.

**Docker Registries**

- Docker image are stored here and can be made public as well as private.
**Docker Objects**

- Objects in Docker are images, containers, volumes, networks.


# Docker Workflow

![Workflow](https://dzone.com/storage/temp/5288806-docker-stages.png)


# Docker Image

- Docker Image consist of environment variables, configuration files, program code, libraries.
 
# Docker Commands

![Commands](https://d2h0cx97tjks2p.cloudfront.net/blogs/wp-content/uploads/sites/2/2018/10/Docker-Architecture-vol.1-01.jpg)
