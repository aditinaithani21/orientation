##GIT SUMMARY

- Git is a version control system.
- we can track the changes made in file.
- .git folder (in the same folder) to store the details of the file.

##Operations possible with git:
1. Check the status of file
2. Restore deleted files
3. Delete added files
4. Track down where a particular line of code was introduced.
5. Undo files changes back to previous versions.

What is Repository?
Folder contains all the data required to track your files and is known as a repository, or repo.

What is Pull?
To Fetch from remote repo to local folder.

What is push?
Opposite of pull we upload local repo file to remote repo.

What is commit?
Used to save changes to the local repo.

##Local Operations:
[](https://www.google.com/search?q=git&sxsrf=ALeKk02uL5QAlkvHlPhMyrZfuOdzUedI5Q:1596691827813&source=lnms&tbm=isch&sa=X&ved=2ahUKEwixo9nJ7IXrAhVY73MBHfPYBk0Q_AUoAnoECCEQBA&biw=2048&bih=1005#imgrc=-Rc-gtCOOWwvdM&imgdii=CljLL3WzvwAKxM)

##Workflow

1. Create a repository(project) using git tool.
2. **Clone** the repository to your local machine.
3. **Add** a file to your local repo
4. **commit** the changes
5. **push** your changes to your master branch
6. **Pull** the changes to your local machine
7. Create a **branch**(version) do changes, commit the changes
8. Open a **pull request** (propose changes to the master branch)
9. **Merge** branch to the master branch

[](https://www.google.com/search?q=git&sxsrf=ALeKk02uL5QAlkvHlPhMyrZfuOdzUedI5Q:1596691827813&source=lnms&tbm=isch&sa=X&ved=2ahUKEwixo9nJ7IXrAhVY73MBHfPYBk0Q_AUoAnoECCEQBA&biw=2048&bih=1005#imgrc=CljLL3WzvwAKxM&imgdii=iXKZdmXoHQ1p4M)


